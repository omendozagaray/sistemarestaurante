﻿using System;
using System.Linq;
using System.Web.Mvc;

namespace SistemaRestaurante.WebSite.Controllers
{
    using SistemaRestaurante.BusinessLogic;
    using SistemaRestaurante.Utils;
    using SistemaRestaurante.WebSite.Core;
    using SistemaRestaurante.BusinessEntity;

    public class CategoriaJqController : BaseController
    {
        public ActionResult Index()
        {
            return View("ListadoJqGrid");
        }


        [HttpPost]
        public JsonResult Listar(GridTable grid)
        {
            var jqgrid = new JQgrid();

            try
            {
                //INICIO CONFG.
                var filtros = Utils.GetWhere(grid.filters, grid.rules);
                var cantidad = CategoriaBL.Instancia.Count(filtros); //Aqui se cambiará por el count respectivo. Por ejm. para Local usar LocalBL.Instancia.Count(filtros)
                var totalPaginas = 0;


                grid.page = (grid.page == 0) ? 1 : grid.page;
                grid.rows = (grid.rows == 0) ? 100 : grid.rows;

                if (cantidad > 0 && grid.rows > 0)
                {
                    var div = cantidad/(decimal) grid.rows;
                    var round = Math.Ceiling(div);
                    totalPaginas = Convert.ToInt32(round);
                    totalPaginas = totalPaginas == 0 ? 1 : totalPaginas;
                }

                grid.page = grid.page > totalPaginas ? totalPaginas : grid.page;

                var start = grid.rows*grid.page - grid.rows;
                if (start < 0) start = 0;

                jqgrid.total = totalPaginas;
                jqgrid.page = grid.page;
                jqgrid.records = cantidad;
                jqgrid.start = start;

                //FIN CONFG.  ---Esto se repetirá comunmente en otras implementaciones para otras clases de modelo.

                var lista = CategoriaBL.Instancia.Listar(grid.rows, start, cantidad, grid.sidx, grid.sord, filtros);

                //recordar que en esta lista el id debe de ser tipo int.  y cell es un array de string.
                jqgrid.rows = lista.Select(item => new JRow
                                                       {
                                                           id = item.IdCategoria,
                                                           cell = new[]
                                                                      {
                                                                          item.IdCategoria.ToString(),
                                                                          item.Nombre,
                                                                          item.Imagen
                                                                      }
                                                       }).ToArray();
            }
            catch (Exception ex)
            {
                MensajeError(ex.Message);
            }

            return Json(jqgrid);
        }

        public ActionResult Crear()
        {
            ViewData["Accion"] = "Crear";

            var entidad = new Categoria();
            PrepararDatos(ref entidad);
            return PartialView("CategoriaInformacion", entidad);
        }

        [HttpPost]
        public JsonResult Crear(Categoria entidad)
        {
            var jsonResponse = new JsonResponse();

            if (ModelState.IsValid)
            {
                try
                {
                    CategoriaBL.Instancia.Agregar(entidad);

                    jsonResponse.Success = true;
                    jsonResponse.Message = "Se Proceso con exito.";
                }
                catch (Exception ex)
                {
                    jsonResponse.Message = ex.Message;
                }
            }
            else
            {
                jsonResponse.Message = "Por favor ingrese todos los campos requeridos";
            }
            return Json(jsonResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Modificar(string id)
        {
            try
            {
                ViewData["Accion"] = "Modificar";

                var entidad = CategoriaBL.Instancia.Obtener(Convert.ToInt32(id));
                PrepararDatos(ref entidad);

                return PartialView("CategoriaInformacion", entidad);
            }
            catch
            {
                return MensajeError();
            }
        }

        [HttpPost]
        public JsonResult Modificar(Categoria entidad)
        {
            var jsonResponse = new JsonResponse();

            if (ModelState.IsValid)
            {
                try
                {
                    var entidadOriginal = CategoriaBL.Instancia.Obtener(entidad.IdCategoria);
                    entidadOriginal.Nombre = entidad.Nombre;
                    entidadOriginal.Imagen= entidad.Imagen;

                    CategoriaBL.Instancia.Modificar(entidadOriginal);

                    jsonResponse.Success = true;
                    jsonResponse.Message = "Se Proceso con exito";
                }
                catch (Exception ex)
                {
                    jsonResponse.Message = ex.Message;
                }
            }
            else
            {
                jsonResponse.Message = "Por favor ingrese todos los campos requeridos";
            }
            return Json(jsonResponse, JsonRequestBehavior.AllowGet);
        }

        private static void PrepararDatos(ref Categoria entidad)
        {
            //TODO: Aqui puede ir elementos que se cargan, como combos, listas, etc.
        }

        [HttpPost]
        public JsonResult Eliminar(string id)
        {
            var jsonResponse = new JsonResponse();

            if (ModelState.IsValid)
            {
                try
                {
                    var categoriaId = Convert.ToInt32(id);
                    CategoriaBL.Instancia.Eliminar(categoriaId);

                    jsonResponse.Success = true;
                    jsonResponse.Message = "Se quito el registro con exito.";
                }
                catch (Exception ex)
                {
                    jsonResponse.Message = "No es posible Eliminar, hay empleados registrados con este cargo. ";
                }
            }
            else
            {
                jsonResponse.Message = "No se pudo eliminar.";
            }
            return Json(jsonResponse, JsonRequestBehavior.AllowGet);
        }
       
    }
}
