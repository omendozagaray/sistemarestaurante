﻿
using System;
using System.Collections.Generic;

using System.Web.Mvc;
using SistemaRestaurante.BusinessEntity;
using SistemaRestaurante.BusinessLogic;
using SistemaRestaurante.Utils;
using SistemaRestaurante.Utils.Enums;

namespace SistemaRestaurante.WebSite.Controllers
{
    public class PedidoAjaxController : Controller
    {
        public IList<DetallePedido> DetallePedidos
        {
            get { return Session["DetallePedido"] as List<DetallePedido>; }
            set { Session["DetallePedido"] = value; }

        } 
        //
        // GET: /PedidoAjax/

        public ActionResult Index()
        {
            var listados = PedidoBL.Instancia.Listar();
            return View(listados);
        }

        public PartialViewResult Create()
        {
            DetallePedidos = new List<DetallePedido>();

            var entidad = new Pedido
            {
                Clientes = ClienteBL.Instancia.Listar(),
                Locales = LocalBL.Instancia.Listar(),
                DetallePedido = DetallePedidos,
                FechaString = DateTime.Now.ToShortDateString(),
                Fecha = DateTime.Now
            };
            PrepararDatos(ref entidad);
            return PartialView(entidad);
        }
        
        [HttpPost]
        public JsonResult Create(Pedido pedido)
        {
            var jsonResponse = new JsonResponse();

            if(ModelState.IsValid)
            {
                try
                {
                    pedido.DetallePedido = DetallePedidos;
                    pedido.Fecha = Convert.ToDateTime(pedido.FechaString);

                    PedidoBL.Instancia.Agregar(pedido);
                    //TODO: insertar en la BD

                    jsonResponse.Success = true;
                    jsonResponse.Message = "Se agregó con Exito";
                }
                catch(Exception ex )
                {
                    jsonResponse.Message = ex.Message;
                }
            }
            return Json(jsonResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreateDetalle()
        {
            var detalle = new DetallePedido
            {
                Productos = ProductoBL.Instancia.Listar(),
                NombreProducto = string.Empty,
            };
            return PartialView(detalle);
        }

        [HttpPost]
        public PartialViewResult CreateDetalle(DetallePedido detalle)
        {
            var item = DetallePedidos.Count + 1;
            var nombreProducto = ProductoBL.Instancia.Obtener(detalle.IdProducto).Nombre;
            detalle.NombreProducto = nombreProducto;
            detalle.IdItem = item;
            DetallePedidos.Add(detalle);
            return PartialView("ListadoDetalle", DetallePedidos);
        }

        public void PrepararDatos(ref Pedido pedido)
        {
            pedido.TiposPedidos = Utils.Utils.EnumToList<TipoPedido>();

        }
    }
}
