﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SistemaRestaurante.BusinessEntity
{
    public class Categoria
    {
        public int IdCategoria { set; get; }
        
        [DisplayName("Nombre de Categoría: ")]
        [Required]
        public string Nombre { set; get; }

        public string Imagen { set; get; }

    }
}
