﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SistemaRestaurante.Utils;
using SistemaRestaurante.Utils.Enums;

namespace SistemaRestaurante.BusinessEntity
{
    public class Pedido
    {
        public int IdPedido { set; get; }
        public int  IdCliente { set; get; }
        public int IdLocal { set; get; }
        public DateTime Fecha { set; get; }

        public string FechaString { set; get; }
        public int TipoPedido { set; get; }

        public IList<DetallePedido> DetallePedido { set; get; }
        public string Cliente { set; get; }
        public string Local { set; get; }

        public IList<Cliente> Clientes { set; get; }
        public IList<Local> Locales { set; get; }

        public IList<Comun> TiposPedidos { set; get; }

    }
}
