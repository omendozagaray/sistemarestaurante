﻿

using System;
using System.Collections.Generic;
using System.Linq;

namespace SistemaRestaurante.Utils
{
    using Newtonsoft.Json;

    public class Utils
    {
        public static List<Comun> EnumToList<T>()
        {
            var enumType = typeof(T);

            if (enumType.BaseType != typeof(Enum))
                throw new ArgumentException("T debe ser de tipo System.Enum");

            var enumValArray = Enum.GetValues(enumType);
            var enumValList = (from object l in enumValArray
                               select new Comun
                               {
                                   Id = Convert.ToInt32(l),
                                   Nombre = Enum.GetName(enumType, l)
                               })
                .OrderBy(p => p.Nombre)
                .ToList();
            return enumValList;
        }

        public static List<Comun> ConvertToComunList<T>(IList<T> list, string value, string text)
        {
            var listItems = (from entity in list
                             let propiedad1 = entity.GetType().GetProperty(value)
                             where propiedad1 != null
                             let valor1 = propiedad1.GetValue(entity, null)
                             where valor1 != null
                             let propiedad2 = entity.GetType().GetProperty(text)
                             where propiedad2 != null
                             let valor2 = propiedad2.GetValue(entity, null)
                             where valor2 != null
                             select new Comun
                             {
                                 Id = Convert.ToInt32(valor1),
                                 Nombre = valor2.ToString()
                             })
                .OrderBy(p => p.Nombre)
                .ToList();
            listItems.Insert(0, new Comun { Nombre = "-- Seleccionar --", Id = 0 });
            return listItems;
        }

        public static string ConvertToSql(Filter filtros)
        {
            var whereFiltro = string.Empty;

            foreach (var filtro in filtros.rules)
            {
                var filterFormatString = string.Empty;

                switch (filtro.op)
                {
                    case "bw":
                        filterFormatString = " {0} {1} like '{2}%'";
                        break; // TODO: might not be correct. Was : Exit Select

                    //equal ==
                    case "eq":
                        filterFormatString = " {0} {1} = {2}";
                        break; // TODO: might not be correct. Was : Exit Select

                    //not equal !=
                    case "ne":
                        filterFormatString = " {0} {1} <> {2}";
                        break; // TODO: might not be correct. Was : Exit Select

                    //string.Contains()
                    case "cn":
                        filterFormatString = " {0} {1} like '%{2}%'";
                        break; // TODO: might not be correct. Was : Exit Select
                    case "dt":

                        filterFormatString = " {0} {1} = DATETIME'{2}'";
                        filtro.data = Convert.ToDateTime(filtro.data).ToString("yyyy-MM-dd HH:mm:ss.fffffff");
                        break; // TODO: might not be correct. Was : Exit Select
                }

                whereFiltro += string.Format(filterFormatString, filtros.groupOp, filtro.field, filtro.data);
            }

            return filtros.rules.Count() == 0 ? string.Empty : whereFiltro.Substring(4);
        }

        public static string GetWhere(string filters, List<Rule> rules)
        {
            var where = string.Empty;
            var filtro = (string.IsNullOrEmpty(filters)) ? null : JsonConvert.DeserializeObject<Filter>(filters);

            if ((filtro != null))
            {
                where = ConvertToSql(filtro);
            }

            if ((rules != null))
            {
                foreach (var regla in rules.Where(regla => (!(string.IsNullOrEmpty(regla.data)) & regla.data != "0")))
                {
                    if (string.IsNullOrEmpty(regla.op))
                    {
                        regla.op = "=";
                    }
                    where += " and " + regla.field + regla.op + regla.data;
                }
            }

            return string.IsNullOrEmpty(@where) ? where : (where.StartsWith(" and ") ? where.Substring(4) : where);
        }

        public static string GetWhere(string columna, string operacion, string valor)
        {
            var opciones = new Dictionary<string, string>
                               {
                                   {"eq", "="},
                                   {"ne", "<>"},
                                   {"lt", "<"},
                                   {"le", "<="},
                                   {"gt", ">"},
                                   {"ge", ">="},
                                   {"bw", "LIKE"},
                                   {"bn", "NOT LIKE"},
                                   {"in", "LIKE"},
                                   {"ni", "NOT LIKE"},
                                   {"ew", "LIKE"},
                                   {"en", "NOT LIKE"},
                                   {"cn", "LIKE"},
                                   {"nc", "NOT LIKE"}
                               };

            if (string.IsNullOrEmpty(operacion))
            {
                return string.Empty;
            }
            else
            {
                if (operacion.Equals("bw") || operacion.Equals("bn"))
                {
                    valor = valor + "%";
                }
                if (operacion.Equals("ew") || operacion.Equals("en"))
                {
                    valor = "%" + valor;
                }
                if (operacion.Equals("cn") || operacion.Equals("nc") || operacion.Equals("in") || operacion.Equals("ni"))
                {
                    valor = "%" + valor + "%";
                }
                if (opciones.Take(6).ToDictionary(p => p.Key, p => p.Value).ContainsKey(operacion))
                {
                    return string.IsNullOrEmpty(valor) ? string.Empty : (columna + " ") + opciones[operacion] + " " + valor;
                }

                return columna + " " + opciones[operacion] + " '" + valor + "'";
            }
        }


    }
}
