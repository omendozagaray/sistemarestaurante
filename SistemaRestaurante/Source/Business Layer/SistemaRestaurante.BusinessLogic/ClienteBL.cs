﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SistemaRestaurante.Utils;

namespace SistemaRestaurante.BusinessLogic
{
    using SistemaRestaurante.BusinessEntity;
    using SistemaRestaurante.DataAcess;

    public class ClienteBL : Singleton<ClienteBL>
    {
        public IList<Cliente> Listar()
        {
            try
            {
                return ClienteDAL.Instancia.Listar();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int Agregar(Cliente cliente)
        {
            try
            {
                return ClienteDAL.Instancia.Agregar(cliente);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Modificar(Cliente cliente)
        {
            try
            {
                return ClienteDAL.Instancia.Modificar(cliente);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Eliminar(int idCliente)
        {
            try
            {
                return ClienteDAL.Instancia.Eliminar(idCliente);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Cliente Obtener(int idCliente)
        {
            try
            {
                return ClienteDAL.Instancia.Obtener(idCliente);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
    }
}
