﻿
using System;
using System.Collections.Generic;
using SistemaRestaurante.BusinessEntity;
using SistemaRestaurante.DataAcess;
using SistemaRestaurante.Utils;

namespace SistemaRestaurante.BusinessLogic
{
    public class PedidoBL : Singleton<PedidoBL>
    {

        public IList<Grafico> PedidosDiarios()
        {
            try
            {
                return PedidoDAL.Instancia.PedidosDiarios();
            }
            catch (Exception ex)
            {
                
                throw new Exception(ex.Message);
            }
        }
        public IList<Pedido> Listar()
        {
            try
            {
                return PedidoDAL.Instancia.Listar();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int Agregar(Pedido pedido)
        {
            try
            {
                return PedidoDAL.Instancia.Agregar(pedido);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Modificar(Pedido pedido)
        {
            try
            {
                return PedidoDAL.Instancia.Modificar(pedido);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Eliminar(int idPedido)
        {
            try
            {
                return PedidoDAL.Instancia.Eliminar(idPedido);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Pedido Obtener(int idPedido)
        {
            try
            {
                return PedidoDAL.Instancia.Obtener(idPedido);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
    }
}
